var CLIENT_ID = '299005720596-ak6cchr1a63je5bcscphustklr9vhdua.apps.googleusercontent.com';
var API_KEY = 'AIzaSyAwqb959B_Gd56QYal-bwiGUJ0XcNt2QBU';
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest", "https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest"];
var SCOPES = "https://www.googleapis.com/auth/calendar.readonly " + "https://www.googleapis.com/auth/youtube.readonly";


  var authorizeButton = document.getElementById('authorize-button');
  var signoutButton = document.getElementById('signout-button');
  var arr=[];
  

  /**
   *  On load, called to load the auth2 library and API client library.
   */
  function handleClientLoad() {
    gapi.load('client:auth2', initClient);
  }

  /**
   *  Initializes the API client library and sets up sign-in state
   *  listeners.
   */
  function initClient() {
    gapi.client.init({
      apiKey: API_KEY,
      clientId: CLIENT_ID,
      discoveryDocs: DISCOVERY_DOCS,
      scope: SCOPES
    }).then(function () {
      // Listen for sign-in state changes.
      gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

      // Handle the initial sign-in state.
      updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
      authorizeButton.onclick = handleAuthClick;
      signoutButton.onclick = handleSignoutClick;
    });
  }

  /**
   *  Called when the signed in status changes, to update the UI
   *  appropriately. After a sign-in, the API is called.
   */
  function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
      authorizeButton.style.display = 'none';
      signoutButton.style.display = 'block';
      listUpcomingEvents();
      //getChannel();
     
       llamarEventos();
       
      
    //  buscar();
       
    // listarVideos();

    } else {
      authorizeButton.style.display = 'block';
      signoutButton.style.display = 'none';
    }
  }

  /**
   *  Sign in the user upon button click.
   */
  function handleAuthClick(event) {
    gapi.auth2.getAuthInstance().signIn();
  }

  /**
   *  Sign out the user upon button click.
   */
  function handleSignoutClick(event) {
    gapi.auth2.getAuthInstance().signOut();
  }


  /**
   * Append a pre element to the body containing the given message
   * as its text node. Used to display the results of the API call.
   *
   * @param {string} message Text to be placed in pre element.
   */
  function appendPre(message) {
    var pre = document.getElementById('content');
    var textContent = document.createTextNode(message + '\n');
    pre.appendChild(textContent);
  }
  
  function listUpcomingEvents() {
    gapi.client.calendar.events.list({
      'calendarId': 'primary',
      'timeMin': (new Date()).toISOString(),
      'showDeleted': false,
      'singleEvents': true,
      'maxResults': 50,
      'orderBy': 'startTime',
    }).then(function(response) {
      var events = response.result.items;
        
      appendPre('Lista de eventos:');

      if (events.length > 0) {
        for (i = 0; i < events.length; i++) {
          var event = events[i];
          var when = event.start.dateTime;
          
            if (!when) {
            when = event.start.date;
          }
          variablea=document.getElementById("buscar").value;
        if (event.summary=== variablea){
            appendPre(event.summary + '......'+' (' + when + ')' + '.....' + event.description)}
        }
      } else {
        appendPre('No existen eventos.');
      }
    });
  }


 
function llamarEventos(){

  var bottonsearch = document.getElementById("search");
  bottonsearch.addEventListener("click",search);
  }
  


function search() {

 var resultados="";
  initMap();
  var divresultados = document.getElementById("results");  
   while(divresultados.hasChildNodes()){
  divresultados.removeChild(divresultados.firstChild);}


  var a= document.getElementById("buscar").value;  
  var b= document.getElementById("cantidad").value;
 
   // prepare the request
     var request = gapi.client.youtube.search.list({
          part: "snippet",
          type: "video",
          q: a,
          maxResults: b         
     }).then(function(response){       
      var resultados = response.result.items;
       for ( var i=0; i<resultados.length; i++){
        var frames = document.createElement("iframe");  
        frames.setAttribute('style',"width:"+  1200/b);
        frames.setAttribute('style',"height:"+ 350/b);
        
         var title=resultados[i].snippet.title;
          var idsvideos=resultados[i].id.videoId;
          frames.setAttribute('src', "https://www.youtube.com/embed/" + idsvideos);
          divresultados.appendChild(frames);
            listarInfo(idsvideos);
     } 
    });
  }
   
  function listarInfo(ids){
    gapi.client.youtube.videos.list({
       id: ids,
       part:'recordingDetails',
       key: API_KEY
      }).then (function(response){
      //documental bipolaridad de mizar  
      var coordenadas=[];
      var longitud ="";
      var latitud ="";  
      var descripcion="";
      var ides=ids;
 if (response.result.items[0].recordingDetails !== 'undefined' ){
   if (response.result.items[0].recordingDetails.location.latitude !== 'undefined' &&  response.result.items[0].recordingDetails.location.longitude !== 'undefined'){
  //descripcion = response.result.items[0].recordingDetails.locationDescription;
    latitud=  response.result.items[0].recordingDetails.location.latitude;       
      longitud= response.result.items[0].recordingDetails.location.longitude;      
   if (longitud !== 'undefined' ||typeof latitud !== 'undefined' ){
      coordenadas.push(latitud , longitud);  
       }
         }//validacion 
        }
        else{    
          coordenadas.push(-1) ;    
          };
          if (coordenadas[0] !== 'undefined' || coordenadas[1]!== 'undefined' ){
            marcadores(coordenadas[0],coordenadas[1]);              
          }
          // arr=coordenadas;            
       console.log(coordenadas);
       }
     );     
  
   }
       
  
 
  
   var marker;
   var map;
   function initMap() {
     map = new google.maps.Map(document.getElementById('map'), {
       zoom: 4,
       center: {lat: 17.0669, lng: -96.7203}
     });
   }
 
   function marcadores(latitud,longitud){
     console.log(latitud,longitud);
    
       marker = new google.maps.Marker({
       map: map,
       draggable: true,
       animation: google.maps.Animation.DROP,
       position: {lat: latitud, lng: longitud}
     });
       marker.addListener('click',function() {
       infowindow.open(map, toggleBounce);
     });
 
     
   function toggleBounce() {
     infowindow.open(map, marker);    
     if (marker.getAnimation() !== null) {
       marker.setAnimation(null);
     } else {
       marker.setAnimation(google.maps.Animation.BOUNCE);
     }
   }
}
