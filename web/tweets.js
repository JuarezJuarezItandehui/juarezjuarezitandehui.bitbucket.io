
  $(function() {
  $("#search").click(function() {
    var caja_texto= document.getElementById("buscar").value;
    mostrarMensaje(caja_texto);
     });   
  
  });
  function mostrarMensaje(caja_texto){
   var parametros= {"query": caja_texto};
  $.ajax({
    data:  parametros,
    url:   'response.php',
    type:  'post'}
  ).success(function(response) {
    console.log("conexion correcta");
    var respuesta= JSON.parse(response);
     var tweets= respuesta.statuses;
     var latitud=0;
     var longitud=0;
    for (var i=0; i<tweets.length; i++){     
      if ( tweets[i].place !== null  ){ 
          longitud= tweets[i].place.bounding_box.coordinates[0][0][0];
          latitud=  tweets[i].place.bounding_box.coordinates[0][0][1];
         //  console.log(latitud,longitud);
           marcadores(latitud,longitud);
           
      }
        
    }//end for
  })
  }


  var markers;
  var mapa;
  function initMap() {
    mapa = new google.maps.Map(document.getElementById('map'), {
      zoom: 2,
      center: {lat: 17.0669, lng: -96.7203}
    });
  }

  function marcadores(latitud,longitud){
    console.log(latitud,longitud);
    var icono = "icon.gif";
   
      markers = new google.maps.Marker({
      map: mapa,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: {lat: latitud, lng: longitud},
      icon: icono
    });
      markers.addListener('click',function() {
      infowindow.open(mapa, toggleBounce);
    });

    
  function toggleBounce() {
    infowindow.open(mapa, markers);    
    if (markers.getAnimation() !== null) {
      marker.setAnimation(null);
    } else {
      markers.setAnimation(google.maps.Animation.BOUNCE);
    }
  }
}
